package com.photoapp.blogdev.unit_integration_testing.controllers;

public class HelloController {

    public String hello(String name) {
        return String.format("Hello, %s", name);
    }
    
}
